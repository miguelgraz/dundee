class Location < ActiveRecord::Base
  EARTH_RADIUS = 6371
  ORIGIN_LAT = 52.53
  ORIGIN_LNG = 13.403
  belongs_to :vehicle
  validate :inside_radius

  def inside_radius
    lat_from = ORIGIN_LAT * Math::PI / 180
    lng_from = ORIGIN_LNG * Math::PI / 180
    lat_to = lat * Math::PI / 180
    lng_to = lng * Math::PI / 180

    angle = Math.asin(Math.sqrt((Math.sin(lat_to - lat_from)**2) + Math.cos(lat_from) * Math.cos(lat_to) * (Math.sin(lng_to - lng_from)**2)))
    errors.add(:base, "Location out of Berlin's boundaries") if angle * EARTH_RADIUS > 3.5
  end
end
