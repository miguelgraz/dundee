class Vehicle < ActiveRecord::Base
  has_many :locations, -> { order(at: :desc) }, dependent: :destroy

  scope :last_positions, -> {
    where(active: true)
    .joins(:locations)
    .where('locations.at = (SELECT MAX(locations.at) FROM locations WHERE locations.vehicle_id = vehicles.id)')
    .select('vehicles.id', 'uuid', 'locations.lat', 'locations.lng')
  }
end
