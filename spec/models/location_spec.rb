require File.expand_path '../../spec_helper.rb', __FILE__

describe "Location" do
  before :each do
    @traveler = Vehicle.create(uuid: 'traveler', active: true)
  end

  it 'accepts locations inside the boundaries' do
    location = @traveler.locations.new(lat: 52.52, lng: 13.416, at: '2017-12-25T12:00:00+01:00')
    expect(location).to be_valid
  end

  it 'disregards locations outside 3.5km radius from origin' do
    location = @traveler.locations.new(lat: 52.38, lng: 13.379, at: '2017-12-25T12:00:00+01:00')
    expect(location).not_to be_valid
    expect(location.errors.full_messages).to eq ["Location out of Berlin's boundaries"]
  end
end
