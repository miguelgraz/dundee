require File.expand_path '../../spec_helper.rb', __FILE__

describe "Vehicle" do
  describe '#last_positions' do
    before :each do
      @actual = Vehicle.create(uuid: 'actual', active: true)
      @actual.locations.create!(lat: 52.51, lng: 13.371, at: '2017-12-21T12:00:00+01:00')
      @actual.locations.create!(lat: 52.52, lng: 13.389, at: '2017-12-25T12:00:00+01:00')
    end

    it 'returns a relation with the latest position of each vehicle' do
      positions = Vehicle.last_positions
      expect(positions.length).to eq 1
      expect(positions.first.attributes).to eq({'id' => @actual.id, 'uuid' => 'actual', 'lat' => 52.52, 'lng' => 13.389})
    end
  end
end
