require File.expand_path '../spec_helper.rb', __FILE__

describe "Dundee requests" do
  describe 'register vehicle' do
    it 'creates a new active vehicle' do
      expect {
        post "/vehicles", {id: 'm1y2u3u4i5d'}.to_json
      }.to change { Vehicle.count }.from(0).to(1)

      vehicle = Vehicle.last
      expect(vehicle.active).to be true
      expect(vehicle.uuid).to eq 'm1y2u3u4i5d'

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end

    it 'marks an existent vehicle as active' do
      vehicle = Vehicle.create!(uuid: 'my_uuid', active: false)

      expect {
        post "/vehicles", {id: 'my_uuid'}.to_json
      }.to change { vehicle.reload.active }.from(false).to(true)

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end

    it 'does nothing without an id' do
      expect {
        post "/vehicles"
      }.not_to change { Vehicle.count }

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end
  end

  describe 'receive location' do
    it 'stores a new location for the vehicle' do
      vehicle = Vehicle.create!(uuid: 'anicecar', active: true)

      expect {
        post "/vehicles/anicecar/locations", {lat: 52.523, lng: 13.414, at: "2017-11-01T21:10:00+01:00"}.to_json
      }.to change { vehicle.locations.count }.from(0).to(1)

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end

    it 'does nothing if the vehicle doesnt exist' do
      expect {
        post "/vehicles/inexistent/locations", {lat: 52.523, lng: 13.414, at: "2017-11-01T21:10:00+01:00"}.to_json
      }.not_to change { Vehicle.count }

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end

    it 'does nothing if the vehicle isnt active' do
      vehicle = Vehicle.create!(uuid: 'inactivecar', active: false)

      expect {
        post "/vehicles/inactivecar/locations", {lat: 52.523, lng: 13.414, at: "2017-11-01T21:10:00+01:00"}.to_json
      }.not_to change { Location.count }

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end

    it 'ignore locations outside Berlins boundaries' do
      vehicle = Vehicle.create!(uuid: 'faraway', active: true)

      expect {
        post "/vehicles/faraway/locations", {lat: 52.47, lng: 13.371, at: "2017-11-01T21:10:00+01:00"}.to_json
      }.not_to change { Location.count }

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end
  end

  describe 'de-register vehicle' do
    it 'marks a vehicle as inactive' do
      vehicle = Vehicle.create!(uuid: 'superactive', active: true)

      expect {
        delete "/vehicles/superactive"
      }.to change { vehicle.reload.active }.from(true).to(false)

      expect(last_response.status).to eq 204
      expect(last_response.body).to be_empty
    end
  end

  describe 'visualize vehicles' do
    it 'responds with 200' do
      get "/where"

      expect(last_response.status).to eq 200
    end
  end

  describe 'fetch locations' do
    it 'returns a JSON with the last position of each vehicle' do
      ally1 = Vehicle.create!(uuid: 'ally1', active: true)
      ally1.locations.create!(lat: 52.55, lng: 13.405, at: '2017-12-02T12:00:00+01:00')
      get '/locations'

      expect(last_response.status).to eq 200
      expect(JSON.parse(last_response.body)).to include({'id' => ally1.id, 'uuid' => "ally1", 'lat' => "52.55", 'lng' => "13.405"})
    end
  end
end
