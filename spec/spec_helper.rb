ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require File.expand_path '../../dundee.rb', __FILE__

module RSpecMixin
  include Rack::Test::Methods
  def app() Dundee end
end

RSpec.configure do |config|
  config.include RSpecMixin

  config.after :all do
    Location.destroy_all
    Vehicle.destroy_all
  end
end
