# Dundee - Tracking the Allygators

Dundee is an approach to solve the [Door2Door's coding challenge](https://github.com/door2door-io/fullstack-code-challenge), it was made with the goals of being as simple/minimal as possible and very well supported by tests.

## Dependencies and setup

It is a Sinatra project using Bundler to manage the few gem dependencies, it depends on having Git, Ruby 2.4.1 and Postgres installed and running.
To start playing with it you can just run:

1. `git clone git@bitbucket.org:miguelgraz/dundee.git`
2. `cd dundee`
3. `bundle`
4. `rake db:create db:migrate`
5. `rackup`

This will fire up the server and expose the API on http://localhost:9292 with three methods available:

* `/vehicles`
to register vehicles and start tracking them

* `/vehicles/:id/locations`
to record a new position for an specific vehicle

* `/vehicles/:id`
to de-register a vehicle and stop accepting new locations for it

Alongside with a map view to check the vehicles' position in real-time, available on http://localhost:9292/where

## Test suite

The project has a test suite built using RSpec that should be installed by running `bundle`, the tests can be executed by simply running `rspec`.

## Running in production

Dundee is also available as a Heroku app, its map can be seen on https://dundee-allygators.herokuapp.com/where. By the time of this writing it intentionally doesn't have any vehicles or locations recorded and therefore won't show any markers.

If desired one can use the [Door2Door Driver Simulator](https://github.com/door2door-io/fullstack-code-challenge/tree/master/driver-simulator) to populate Dundee running on Heroku, this can be achieved by starting the simulator and pointing it to the Heroku instance of Dundee with `yarn start dundee-allygators.herokuapp.com:80`.

## Notes taken while developing

### Why Sinatra:
I've been meaning to play with it again for a while;
It seemed like a good fit for the size of the challenge, in terms of being able to keep it short and simple.

### Why ActiveRecord:
It's the ORM I'm more comfortable with and the challenge doesn't seem too complex to require any extras.

### Why decimal for lat/long:
Given the relative shortness of the maximum distance of 3.5km I chose to use flat calculations with the [Haversine formula](https://rosettacode.org/wiki/Haversine_formula) and therefore something like [PostGis](http://postgis.net/) seemed like a big overkill for this.

### Why an extra field for vehicles' `UUID`:
The challenge makes it clear that the the vehicles already and always send their own UUID, the vehicle can exit the city's boundaries and re-enter or de-register to register again so I chose to have another regular ID for them and deal with the `active` column instead of messing up with the data, also to avoid losing data reference when i.e. analysing locations for a vehicle that was removed from the database altogether;

The database column ended up being a regular string for flexibility.

### Why manually destroying Locations and Vehicles on `Rspec.configure`:
I didn't want to add another dependency and given the current complexity of the project and the tests this seemed like a good solution to work have a clean test database state.

### Not sure what the challenge description means by "Documentation":
I'm aiming to keep the codebase as simple and readable as possible, to have really clear tests so one can read/run the tests and understand the goals, and of course to have a descriptive README with instructions for everyone to run the project. I'm not planning on writing why/how to use the API though as the challenge description have that clear enough already.

### Improving the map view:
A nice improvement on the map management would be to keep track of markers and just update their locations instead of destroying and recreating them, probably keeping the connection open using a websocket.

Another nice improvement to display the map handling a higher volume of vehicles would be to only create/display markers for the vehicles that are in the current map visualization, checking the current map position and zoom, disregarding the markers out of that visualization.

Yet another point to improve in order to handle more vehicles having their positions updated would be finding a way to replace the subquery `SELECT MAX(locations.at) FROM locations WHERE locations.vehicle_id = vehicles.id` on `Vehicle.last_positions` with a faster approach that wouldn't lose performance dealing with a bigger `locations` table. Perhaps storing the last `location_id` on the `vehicles` table or, depending on the need, also storing the latest lat/lng directly on the `vehicles` table, at the cost of sacrificing some of the normalization for a faster query.

### Dockerizing
As I'm not that familiar with Docker and I was already taking a long time to finish this test I chose not to write a Dockerfile without fully understanding it.
