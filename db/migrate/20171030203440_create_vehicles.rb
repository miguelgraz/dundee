class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string :uuid, null: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
