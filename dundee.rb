require 'bundler'
Bundler.require

require './models/vehicle'
require './models/location'

class Dundee < Sinatra::Application
  before do
    body = request.body.read
    params.merge! JSON.parse(body) if body.present?
  end

  post '/vehicles' do
    if params[:id].present?
      vehicle = Vehicle.find_or_initialize_by(uuid: params[:id])
      vehicle.active = true
      vehicle.save
    end

    204
  end

  post '/vehicles/:id/locations' do
    vehicle = Vehicle.find_by(uuid: params[:id], active: true)
    vehicle.locations.create(lat: params[:lat], lng: params[:lng], at: params[:at]) if vehicle

    204
  end

  delete '/vehicles/:id' do
    vehicle = Vehicle.find_by(uuid: params[:id], active: true)
    vehicle.update(active: false) if vehicle

    204
  end

  get '/where' do
    erb :where
  end

  get '/locations' do
    content_type :json
    Vehicle.last_positions.to_json
  end
end
